const recipe = require("./recipe/recipe");

module.exports = app => {
  app.use("/recipe", recipe);
};
