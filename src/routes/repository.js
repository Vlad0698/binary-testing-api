class Repository {
  constructor(model) {
    this.model = model;
  }

  findAll() {
    return this.model.find().exec();
  }

  findById(id) {
    return this.model.findById(id).exec();
  }

  create(data) {
    console.log(this.model.create);
    return this.model.create(data);
  }
}

module.exports = Repository;
