class Service {
  constructor(repository) {
    this.repository = repository;
  }

  findAll() {
    return this.repository.findAll();
  }

  findById(id) {
    return this.repository.findById(id);
  }

  create(data) {
    return this.repository.create(data);
  }
}

module.exports = Service;
