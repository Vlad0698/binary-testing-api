const Service = require("../service");
const repository = require("./recipeRepository");

class RecipeService extends Service {}

module.exports = new RecipeService(repository);
