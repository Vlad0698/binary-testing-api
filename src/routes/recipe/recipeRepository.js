const Repository = require("../repository");
const Recipe = require("../../models/recipe");

class RecipeRepository extends Repository {}

module.exports = new RecipeRepository(Recipe);
