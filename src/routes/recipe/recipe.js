const express = require("express");
const recipeService = require("./recipeService");
const recipe = express.Router();

recipe.route("/").get((req, res) => {
  recipeService
    .findAll()
    .then(recipes => {
      console.log(recipes);
      return res.status(200).send(recipes);
    })
    .catch(err => {
      return res.status(err.status).send(err.message);
    });
});

recipe.route("/").post((req, res) => {
  const recipe = req.body;
  recipeService
    .create(recipe)
    .then(result => {
      console.log(result);
      return res.status(200).send(result);
    })
    .catch(err => {
      return res.status(err.status).send(err.message);
    });
});

module.exports = recipe;
