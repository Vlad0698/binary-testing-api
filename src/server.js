const express = require("express"),
  app = express(),
  server = require("http").Server(app);

require("dotenv").config();
const { host, port } = require("./config/env");
require("./config/db");
require("./middleware")(app);
require("./routes")(app);

server.listen(port, () => console.log(`server live on ${host}:${port}`));
