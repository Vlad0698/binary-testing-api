const mongoose = require("mongoose");

const { String } = mongoose.Schema.Types;

const definition = {
  title: String,
  description: String,
};

const options = {
  id: true,
  timestamps: true,
};

const schema = new mongoose.Schema(definition, options);

const Recipe = mongoose.model("Recipe", schema);

module.exports = Recipe;
