const mongoose = require("mongoose");
const { user, password } = require("./env").db;
const opts = {
  auto_reconnect: true,
  useNewUrlParser: true,
};
const uri = `mongodb+srv://${user}:${password}@binarytest-pfrgi.gcp.mongodb.net/binary-test?retryWrites=true&w=majority`;

const connectToDb = () => {
  mongoose.connect(uri, opts);
  const db = mongoose.connection;
  db.on("connected", () => console.log("connected to the database"));
  db.on("disconnected", () => console.log("disconnected from the database"));
  db.on("error", err => console.error(err));
};

process.on("SIGINT", () => {
  mongoose.connection.close(() => {
    process.exit(0);
  });
});

module.exports = connectToDb();
