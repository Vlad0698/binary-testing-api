module.exports = {
  port: process.env.PORT || 5000,
  host: process.env.HOST || "http://127.0.0.1",
  db: {
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
  },
};
